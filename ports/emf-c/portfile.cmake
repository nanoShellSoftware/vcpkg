#object library
include(vcpkg_common_functions)

vcpkg_from_gitlab(
    GITLAB_URL https://gitlab.com
    OUT_SOURCE_PATH SOURCE_PATH
    REPO nanoShellSoftware/emf-interface-c
    REF 0009a8e3fb6ebbd7a72cbf9f0689760a9ab61dfb
    SHA512 ac3217ce6f1fb39ebaa969196125fd43678d5ee70c45eb8d8cc8fe461a8505df7293f513a08e4281b0037f507c0b6730d9dcf0f42f7aeb66db3b419b1cbb12e6
    HEAD_REF master
)

vcpkg_configure_cmake(
    SOURCE_PATH ${SOURCE_PATH}
    PREFER_NINJA
    OPTIONS
        -DEMF_BUILD_TESTS:BOOL=OFF
        -DEMF_BUILD_DOCS:BOOL=OFF
        -DEMF_DISABLE_OPTIMIZATIONS:BOOL=OFF
        -DEMF_ENABLE_NATIVE_ARCH:BOOL=OFF
        -DEMF_ENABLE_CLANG_TIDY:BOOL=OFF
        -DCMAKE_INSTALL_DIR:STRING=cmake
)
vcpkg_install_cmake()

file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/debug/include")
file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/debug/share")

#file(REMOVE_RECURSE ${CURRENT_PACKAGES_DIR}/lib ${CURRENT_PACKAGES_DIR}/share/doc)
file(INSTALL ${SOURCE_PATH}/LICENSE DESTINATION ${CURRENT_PACKAGES_DIR}/share/${PORT} RENAME copyright)